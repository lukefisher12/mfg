<?php


class CoreUserController {
	
	// Declare Variables used in class
	private static $CoreUserModel;
	private static $cacheFileLoc;
	
	
	function __construct() {
	
		$this->CoreUserModel = new CoreUserModel();
		$this->cacheFileLoc = $_SERVER['DOCUMENT_ROOT'] . '/mfg/Core/Cache/CoreUsers.txt';
	}
	
	public function getCoreUsersCTRL() {
		// Check if cache has been created
		if(file_exists ($this->cacheFileLoc)){
			$file = fopen($this->cacheFileLoc, "r");
			$data = fread($file, filesize($this->cacheFileLoc)); 
			$data = unserialize($data);
			$data['source'] = 'Cache';
			fclose($readData);
		} else {
			$data = $this->CoreUserModel->getCoreUsers(); 
			// Create cache 
			$file = fopen($this->cacheFileLoc,"a");
			$fwrite = fwrite($file, serialize($data));
			$data['source'] = 'Database';
			fclose($file); 
		}
		// Data will be pulled from either cache or curl call.
		return $data;
	}
	
	
	//Remove Cache File
	public function removeCoreUsersCacheCTRL() {
		if(file_exists ($this->cacheFileLoc)) {
			$fileDelete = unlink($this->cacheFileLoc);
			if($fileDelete) {
				$rt['status'] = 'success';
				$rt['response'] = 'Cached file removed';
			} else {
				$rt['status'] = 'error';
				$rt['response'] = 'File exsists, but could not be deleted';
			}
		} else {
			$rt['status'] = 'error';
			$rt['response'] = 'File does not exsist';
		}
		return $rt;
	}
}

?>