<?php




spl_autoload_register(function ($class_name) {
	
	$filename = $_SERVER['DOCUMENT_ROOT'] . '/mfg/Core/Controllers/' . $class_name ."Class.php";
	if (file_exists($filename)) include_once($filename);
	$filename = $_SERVER['DOCUMENT_ROOT'] . '/mfg/Core/Models/' . $class_name ."Class.php";
	if (file_exists($filename)) include_once($filename);
	
	// Change to try catch
});

