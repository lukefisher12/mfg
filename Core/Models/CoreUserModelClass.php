<?php

// Error Code 4 = Unexpected return

class CoreUserModel extends BaseModel {
	
	function __construct() {
			 
	}

	// Create Curl Call 
	public function getCoreUsers() {
		$curl = curl_init();
		$postValues = array(
			'mgf' => 'userData',
			'apiKey' => '123455678qwertyui'
		);
		curl_setopt($curl, CURLOPT_URL, "http://www.mgf.ltd.uk/software-test/api.php"); 
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_POSTFIELDS,
            $postValues);
        $output = curl_exec($curl); 
        curl_close($curl);
		
		$rt = array();
		if($output != '' && gettype($output) == 'string') {
			$rt['status'] = 'success';
			$rt['response'] = $output;
		} else {
			$rt['status'] = 'failed';
			$rt['response'] = 'Error Code 4';
		}
		return $rt;
	} 
	
	 
	
}

 





?>