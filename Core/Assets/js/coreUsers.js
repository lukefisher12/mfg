// Global Variables
var coreUsers;
var JsonData;
var XMLData;
var csvData;
 

// Ajax Call, to obtain user data
var getUser = function() {
	var request = $.ajax({
		url: "../API/API.php",
		method: "POST",
		data: {f : 'getCoreUsers'},
		dataType: 'json',
	}) 
	  .done(function(userRes) {
		$("#engineersData").empty();
		$("#internalData").empty();
		$("#externalData").empty();
		JsonData = userRes.response;
		XMLData = format.xml(userRes.response);
		csvData = format.csv(userRes.response);
		coreUsers = JSON.parse(userRes.response);
		$.each(coreUsers, function(userType, users) {
			var users = new newUserType(userType, users) 
			users.sortUsers();
			coreUsers[users.type].averageSalary = users.averageSalary();
			coreUsers[users.type].averageAge = users.averageAge();
		})
		swal("Data Received!", "Source " + userRes.source, 'success')
		$('.well').fadeIn(300);
		$('.dataType').fadeIn(300);
	  })
	  .fail(function(msg) {
		swal("No Data Received!", "Check Console", "error");
		console.log(msg);
	  })
}

// Allows creation of a user type instance
function newUserType(userType, users) {
	this.type = userType;
	this.userGroup = users;
}


// works out user group average salary
newUserType.prototype.averageSalary = function() {
	var salaryTotal = 0;
	$.each(this.userGroup, function(index, user) {
		salaryTotal = salaryTotal + parseFloat(user.salary);
	});
	salaryTotal = parseInt(salaryTotal / Object.keys(this.userGroup).length);
	if(isNaN(salaryTotal)) salaryTotal = false;
	return salaryTotal;
}


// works out user group average age
newUserType.prototype.averageAge = function() {
	if(this.type == 'internal') {
		var format = 'DD-MM-YYYY';
	} else {
		var format = 'DD-MM-YY';
	}
	var ageDifference = 0
	$.each(this.userGroup, function(index, user) {
		if(typeof user == 'object') {
			user.DOB = moment(user.DOB,format)
				ageDifference = ageDifference - moment().diff(user.DOB); 
		}
	});
	ageDifference = ageDifference / 3;
	ageDifference = moment().millisecond(ageDifference)
	return {
		year: ageDifference.format('YYYY'),
		month: ageDifference.format('MMMM'),
		day: ageDifference.format('DD'),
	}
}

// Inserts data into html 
newUserType.prototype.sortUsers = function() {
	if(this.type == 'engineers') {
		$.each(this.userGroup, function(index, user) {
			$("#engineersData").append("<tr><td>" + user.firstName + "</td> <td>" + user.lastName + "</td> <td>" + user.DOB + "</td> <td>" + user.email + "</td> <td>" + user.password + "</td> <td>" + user.qualifications + "</td> <td>" + user.depot + "</td> <td>" + user.field + "</td> <td>" + user.level + "</td> <td>" + user.salary + "</td> <td>" + user.payrollID + "</td></tr>");
		})
	} else if(this.type == 'internal') {
		$.each(this.userGroup, function(index, user) {
			$("#internalData").append("<tr><td>" + user.firstName + "</td> <td>" + user.lastName + "</td> <td>" + user.DOB + "</td> <td>" + user.email + "</td> <td>" + user.password + "</td>  <td>" + user.salary + "</td> <td>" + user.payrollID + "</td></tr>");	
		})
	} else if(this.type == 'external') {
		$.each(this.userGroup, function(index, user) {
			$("#externalData").append("<tr><td>" + user.firstName + "</td> <td>" + user.lastName + "</td> <td>" + user.DOB + "</td> <td>" + user.email + "</td> <td>" + user.password + "</td> </tr>");	
		})
	}else {
		//throw error
	}
} 


// Ajax request to clear cache
var removeUserCache = function() {
	var request = $.ajax({
		url: "../API/API.php",
		method: "POST",
		data: {f : 'removeCoreUsersCache'},
		dataType: 'json',
	}) 
	  .done(function(cacheRemoveRes) {	
		if(cacheRemoveRes.status == 'success') {
			swal(cacheRemoveRes.response,'','success')
		} else {
			swal(cacheRemoveRes.response,'','error')
		}
	  })
	  .fail(function(msg) {
		swal("No Data Received!", "Check Console", "error"); 
		console.log(msg);
	  })
}

// on change format
$( "#dataFormatSelect" ).change(function() {
	$('#formatDisplay').fadeIn(300);
	$('#formatDisplay').empty();
	$('#formatDisplay').append("<textarea rows='6' class='form-control'>" + window[$(this).val()] + "</textarea>"); 
});

// encapsulates formatting functions
var format = {
	xml: function(value) {
		value = JSON.parse(value);
		var x2js = new X2JS();
		var xml = x2js.json2xml_str( value );
		return xml
	},
	csv: function(value) {
            var array = typeof value != 'object' ? JSON.parse(value) : value;
            var str = '';
            for (var i = 0; i < array.length; i++) {
                var line = '';
                for (var index in array[i]) {
                    if (line != '') line += ','

                    line += array[i][index];
                }
                str += line + '\r\n';
            }
            return str;
	}
}